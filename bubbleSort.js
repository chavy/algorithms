// const bubbleSort = (array) => {
//     for (let i = 0; i < array.length; i++) {
//         const currentElement = array[i];
//         if (currentElement > array[i + 1]) {
//             array[i] = array[i + 1];
//             array[i + 1] = currentElement;
//             i = -1;
//         }
//     }
//     return array;
// };

const bubbleSort = (array) => {
    for (let i = array.length; i > 0; i--) {
        for (let j = 0; j < i; j++) {
            if (array[j] > array[j + 1]) {
                const temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
    return array;
}

console.log(bubbleSort([4, 3, 2, 5, 1, 5, 9]));
