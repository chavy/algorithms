const maxStockProfit = (priceArr) => {
    let maxProfit = -1;
    let buyPrice = 0;
    let sellPrice = 0;

    let changeBuyPrice = true;

    for (let i = 0; i < priceArr.length; i++) {
        if (changeBuyPrice) buyPrice = priceArr[i];
        sellPrice = priceArr[i + 1];

        if (sellPrice < buyPrice) {
            changeBuyPrice = true;
        }
        else {
            const tempProfit = sellPrice - buyPrice;
            if (tempProfit > maxProfit) maxProfit = tempProfit;
            changeBuyPrice = false;
        }
    }
    return maxProfit;
};

console.log(maxStockProfit([10, 18,4,5,9,6,16,12]));
