const reverseArrayInPlace = (arr) => {
    let lastChanged = arr.length - 1;
    for (let i = 0; i < arr.length / 2; i++) {
        let currentElement = arr[i];
        arr[i] = arr[lastChanged];
        arr[lastChanged] = currentElement;
        lastChanged--;
    }
    return arr;
};

console.log(reverseArrayInPlace([1, 2, 3, 4, 5, 6]));