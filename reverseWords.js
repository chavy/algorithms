const reverseWords = (string) => {
    string = string.split(' ');
    let reversedWordsString = '';
    string.forEach(word => {
        let newWordArr = [];
        for (let j = word.length; j > -1; j--){
            newWordArr.push(word[j]);
        }
        reversedWordsString += newWordArr.join('') + ' ';
    });
    
    return reversedWordsString;
}

console.log(reverseWords('this is a string of words'));