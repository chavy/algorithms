// Logarithmic runtime
// Big O Notation: O (log n)
const binarySearch = (array, key) => {
    let low = 0;
    let hight = array.length -1;
    let mid;
    let element;

    while (low <= high) {
        mid = Math.floor((low + high) / 2, 10);
        element = array[mid];
        if(element < key) {
            low = mid +1;
        } else if (element > key) {
            high = mid - 1;
        } else {
            return mid;
        }
    }
    return -1;
}